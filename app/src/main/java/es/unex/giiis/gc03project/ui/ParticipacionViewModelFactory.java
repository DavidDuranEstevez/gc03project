package es.unex.giiis.gc03project.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.gc03project.ParticipacionRepository;

public class ParticipacionViewModelFactory extends ViewModelProvider.NewInstanceFactory{
    private final ParticipacionRepository mRepository;

    public ParticipacionViewModelFactory(ParticipacionRepository repository){
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new ParticipacionViewModel(mRepository);
    }
}
