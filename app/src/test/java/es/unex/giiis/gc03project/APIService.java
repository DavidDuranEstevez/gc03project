package es.unex.giiis.gc03project;

import es.unex.giiis.gc03project.network.OpenDataService;
import es.unex.giiis.gc03project.objects.Pistas;

import org.junit.Test;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertTrue;

public class APIService {

    @Test
    public void crearLlamadaAPITest() throws IOException{

        String nombrePrimeraPista = "Espacio deportivo instalación: Ciudad Deportiva De La Junta De Extremadura";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://opendata.caceres.es/GetData/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OpenDataService service = retrofit.create(OpenDataService.class);//Retrofit me crea instancia de interfaz de arriba
        Call<Pistas> call = service.cogerPistas();

        Response<Pistas> response = call.execute();
        Pistas p = response.body();

        assertTrue(response != null);
        assertTrue(p.getResults().getBindings().get(0).getFoafName().getValue().equals(nombrePrimeraPista));

    }
}
